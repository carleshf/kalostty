#!/usr/bin/env ruby
# encoding: UTF-8

def split_lines(text, width)
  lines = []
  if text then
    begin
      i = width
      if text.length <= i then # text length is OK -> add it to array and stop splitting
        lines << text
        text = ""
      else
        # search for word boundary (space actually)
        while i > 0 and text[i] != ' '[0] do
          i -= 1
        end
        # if we can't find any space character, simply cut it off at the maximum width
        if i == 0 then
          i = width
        end
        # extract line
        x = text[0..i-1]
        # remove extracted line
        text = text[i+1..-1]
        # added line to array
        lines << x
      end
    end while text.length > 0
  end
  return lines
end



# load_ncurses
# -----------------------------------------------------------------------------
# Loads the ncurses-ruby module and imports "Ncurses".
def load_ncurses
  begin
    require "ncurses"
    include Ncurses
  rescue LoadError
    puts "ERROR no 'ncurses' available."
    exit(1)
  end
end


# Slide
# -----------------------------------------------------------------------------
# Represents a slide in the slideshow. It is composed by a title (aka.
# caption) and a list of lines (aka. content)
class Slide
  attr_accessor :caption, :nslide, :pbar

  def initialize(nslide)
     @caption = ""
     @nslide  = nslide
     @content = []
     @current_line = 0
     @eos = false
     @pbar = true
  end

  def set_pbar(status)
    @pbar = status
  end

  def set_caption(caption)
    @caption = caption
  end

  def add_line(line)
    if line =~ /^\\caption /
       @caption = line.sub(/^\\caption /, "")
    else
      @content = @content.push(line)
    end
  end

  def next_line
    line = @content[@current_line]
    @current_line += 1
    if @current_line >= @content.size
      @eos = true
    end
    return line
  end

  def eos?
    return @eos
  end
end


# SlideShow
# -----------------------------------------------------------------------------
# Represents the complete set of Slides and includes the title, the subtitle,
# the author, teh afiliation and the date of the 'slideshow'.
class SlideShow

  attr_accessor :slides

  def initialize(filename, header, slides)
    @filename = filename
    @slides   = slides
    process_header(header)
  end

  def process_header(header)
    title = nil
    subtitle = nil
    author = nil
    afiliation = nil
    date = nil
    header.each do |line|
      case line
        when /^\\title /
          title = line.sub(/^\\title /, "")
        when /^\\subtitle /
          subtitle = line.sub(/^\\subtitle /, "")
        when /^\\author /
          author = line.sub(/^\\author /, "")
        when /^\\afiliation /
          afiliation = line.sub(/^\\afiliation /, "")
        when /^\\date /
          date = line.sub(/^\\date /, "")
      end
    end
    sld = Slide.new 0
    sld.add_line("")
    sld.add_line(title)
    sld.add_line(subtitle)
    sld.add_line("")
    sld.add_line(author)
    sld.add_line(afiliation)
    sld.add_line("")
    sld.add_line(date)
    sld.set_pbar(false)
    @slides.unshift(sld)
  end

  def start
    @figletfont = "standard"
    Ncurses.initscr
    Ncurses.curs_set(0)
    Ncurses.cbreak
    Ncurses.noecho
    Ncurses.stdscr.intrflush(false)
    Ncurses.stdscr.keypad(true)
    Ncurses.start_color()
    Ncurses.use_default_colors()
    ##do_bgcolor("black")
    #do_fgcolor("white")
    ##@fgcolor = ColorMap.get_color_pair("white")
    @screen = Ncurses.stdscr
    @termwidth = Ncurses.getmaxx(@screen)
    @termheight = Ncurses.getmaxy(@screen)
    @vindent = 1
    @hindent = 3
    @cur_line = @vindent
  end

  def close
    Ncurses.nocbreak
    Ncurses.endwin
  end

  def set_indents(hindent, vindent)
    @hindent = hindent
    @vindent = vindent
  end

  def clear
    @screen.clear
    @screen.refresh
  end

  def print_line(line)
    width = @termwidth - 2 * @hindent
    lines = split_lines(line, width)
    lines.each do |l|
      if @cur_line < @termheight - @vindent - 2
        @screen.move(@cur_line, @hindent)
        @screen.addstr(l)
        @cur_line += 1
      end
    end
  end

  def hline(char="-")
    width = @termwidth - 2 * @hindent
    ln = char * width
    print_line(ln)
    @cur_line += 1
  end

  def process_caption(caption)
    if !caption.nil? and caption != ""
      print_line(caption)
      hline
    end
  end

  def process_footer(nslide, tslides)
    width = @termwidth - 2 * @hindent
    nslide += 1

    nn = tslides.to_s.length
    ns = nslide.to_s.rjust(nn, "0") + "/" + tslides.to_s.rjust(nn, "0") + " "
    pp = (nslide / (tslides * 1.0) * 100).round(0)
    np = " " + pp.to_s.rjust(3, " ") + "%"

    wd = width - ns.length - np.length - 2
    pp = wd * pp / 100

    pbar = ns + "[" + "=" * pp + " " * (wd - pp) + "]" + np

    @screen.move(@termheight - @vindent - 1, @hindent)
    @screen.addstr(pbar)
  end

  def process_line(line)
    case line
    when /^\\hrule /
        print_line(line.sub(/^\\title /, ""))
      when /^\\bgcolor /
        print_line(line.sub(/^\\author /, ""))
      when /^\\fontcolor /
        print_line(line.sub(/^\\date /, ""))
      when /^\\center /
      when /^\\right /
      when /^\\left /
      when /^\\bfon /
      when /^\\bfoff /
      when /^\\ulon /
      when /^\\uloff /
      when /^\\pause/
        get_key
      else
        print_line(line)
    end
  end

  def get_key
    ch = @screen.getch
    case ch
      when Ncurses::KEY_RIGHT
        return :keyright
      when Ncurses::KEY_DOWN
        return :keydown
      when Ncurses::KEY_LEFT
        return :keyleft
      when Ncurses::KEY_UP
        return :keyup
      when Ncurses::KEY_RESIZE
        return :keyresize
      else
        return ch
    end
  end

  def visualize
    @slides.each do |sld|
      process_caption(sld.caption)
      begin
        process_line(sld.next_line)
      end while !sld.eos?
      if sld.pbar
        process_footer(sld.nslide, @slides.size)
      end
      get_key
      empty_slide
    end
  end

  def empty_slide
    clear
     @cur_line = @vindent
  end

end


def parse_file(filename)
  header = []
  slides = []
  nslide = 1
  beg    = false
  sld    = nil

  fh = File.open(filename)
  fh.each_line do |line|
    line.chomp
    case line
      when /^%/ # if comment nothing to do
      when /^\\begin/ # need begin to start creating slides
        sld = Slide.new(nslide)
        beg = true
      when /^\\newslide/
        if beg
          slides = slides.push(sld)
          nslide += 1
          sld = Slide.new(nslide)
        else
          puts "Starting document without '\\begin'."
          exit(1)
        end
      else
        if beg
          sld.add_line(line)
        else
          header = header.push(line)
        end
    end
  end
  slides = slides.push(sld)
  return SlideShow.new(filename, header, slides)
end


x = parse_file("examples/test1.kl")
#puts x
load_ncurses
x.start
x.visualize
x.close
