#!/usr/bin/env python
# encoding: UTF-8

import argparse
import curses
import sys
import time

## ----------------------------------------------------------------------------

COLORS = {
    'black':   0, 
    'red':     1, 
    'green':   2, 
    'yellow':  3, 
    'blue':    4, 
    'magenta': 5, 
    'cyan':    6,
    'white':   7,
}

## ----------------------------------------------------------------------------

def split_lines(text, width, align="left"):
    lines = []
    if text != '':
        while len(text) > 0:
            if len(text) <= width:
                lines.append(text)
                text = ''
            else:
                ii = width
                while ii > 0 and text[ii] != ' ':
                    ii -= 1
                if ii == 0:
                    ii = width
                x = text[0:ii]
                text = text[ii:].strip()
                lines.append(x)
    else:
        lines.append('')

    for ii,x in enumerate(lines):
        if x == '': continue
        if align == 'right':
            lines[ii] = " " * (width - len(x)) + x
        elif align == 'center':
            n = int(round((width - len(x)) / 2, 0))
            lines[ii] = " " * n + x + " " * (width - n)

    return lines

## ----------------------------------------------------------------------------

class KalosSlide:
    """Representation of a single slide composed by a caption and list of 
    strings as content. As footer it can shows the slide number in the  show
    and a percentage bar."""

    def __init__(self, nslide):
        """Constructor of the class."""
        self.caption = ""
        self.nslide = nslide
        self.pbar = True 
        self.content = []
        self.current_line = 0
        self.eos = False
        self.fgcolor = 0
        self.bgcolor = 7

    def set_pbar(self, status = True):
        """Sets to true/false the property that shows or not shows the 
        percentage bar and the slide number at the footer of the slide."""
        self.pbar = status

    def set_caption(self, caption):
        """Sets the title of the slide."""
        self.caption = caption

    def add_line(self, line, raw_mode=False):
        """Adds a line to the slide content. It checks is the line starts
        with '--caption' to set the slide caption."""
        if line.startswith('--caption') and not raw_mode:
            self.set_caption(line.replace('--caption ', ''))
        else:
            self.content.append(line)

    def next_line(self):
        """Returns the next slide line if no EOS reached, otherwise returns
        False."""
        if self.eos:
            return False

        line = self.content[self.current_line]
        self.current_line += 1
        if self.current_line >= len(self.content):
            self.eos = True
        return line

    def is_eos(self):
        """Returns True if the EOS was reached, otherwise returns False."""
        return(self.eos)

    def set_colors(self, fg, bg):
        self.fgcolor = fg
        self.bgcolor = bg

## ----------------------------------------------------------------------------

class KalosShow:
    """Representation of a slide show under terminal environment."""

    def __init__(self, file_name, fgcolor = None, bgcolor = None):
        """Constructor of the class."""
        self.slides = []
        self.stdscr = None
        self.title = ''
        self.subtitle = ''
        self.author = ''
        self.date = ''
        self.affiliation = ''
        self.align = 'left'
        self.bgcolor = COLORS["white"] if bgcolor is None else bgcolor
        self.fgcolor = COLORS["black"] if fgcolor is None else fgcolor
        self.txt_bgcolor = COLORS["white"] if bgcolor is None else bgcolor
        self.txt_fgcolor = COLORS["black"] if fgcolor is None else fgcolor
        self.raw_mode = False
        self.load_file(file_name)

    def load_file(self, file_name):
        """Given a file's name it opens the file and it reads the content to 
        create a slide-show."""
        fh = open(file_name, 'r')
        portrait = KalosSlide(0)
        portrait.set_colors(self.fgcolor, self.bgcolor)
        current_slide = KalosSlide(0)
        current_slide.set_colors(self.fgcolor, self.bgcolor)
        for line in fh.readlines():
            line = line.strip('\n').strip('\r')
            
            if self.raw_mode and not line.startswith('--raw_mode_off'):
                current_slide.add_line(line, True)
            elif line.startswith('--#'):
                continue
            elif line.startswith('--title'):
                self.title = line.replace('--title ', '')
            elif line.startswith('--subtitle'):
                self.subtitle = line.replace('--subtitle ', '')
            elif line.startswith('--author'):
                self.author = line.replace('--author ', '')
            elif line.startswith('--date'):
                date = line.replace('--date ', '')
                if date.startswith('@today'):
                    form = date.replace('@today ', '').replace('@today', '')
                    if form == "":
                        form = "%d/%m/%Y" ## dd/mm/yyyy format
                    date = time.strftime(form)
                self.date = date
            elif line.startswith('--affiliation'):
                self.affiliation = line.replace('--affiliation ', '')
            elif line.startswith('--newslide'):
                self.slides.append(current_slide)
                current_slide = KalosSlide(len(self.slides))
                current_slide.set_caption(
                    line.replace('--newslide ', '').replace('--newslide', '')
                )
                current_slide.set_colors(self.fgcolor, self.bgcolor)
            elif line.startswith('--bgcolor'):
                bgcolor = line.replace('--bgcolor ', '')
                current_slide.bgcolor = COLORS[bgcolor]
                self.bgcolor = current_slide.bgcolor
            elif line.startswith('--fgcolor'):
                fgcolor = line.replace('--fgcolor ', '')
                current_slide.fgcolor = COLORS[fgcolor]
                self.fgcolor = current_slide.fgcolor
            elif line.startswith('--raw_mode_on'):
                self.raw_mode = True
                current_slide.add_line(line)
            elif line.startswith('--raw_mode_off'):
                self.raw_mode = False
                current_slide.add_line(line)
            else:
                current_slide.add_line(line)

        self.slides.append(current_slide)        
        fh.close()
        self.create_portrait(portrait)

    def start(self):
        """Initialize the curses and setup the terminal environment for the
        slide-show."""
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.curs_set(False) # ..... No cursor
        self.stdscr.keypad(True) # ... Get special keys charset
        curses.start_color()
        #curses.use_default_colors()
        self.mod = {'colors': 1, 'modifier': (False, )}
        self.stdscr.bkgd(' ', curses.color_pair(1))
        self.theight, self.twidth = self.stdscr.getmaxyx()
        self.vindent = 1
        self.hindent = 3
        self.current_line = self.vindent
        curses.init_pair(1, self.fgcolor, self.bgcolor)

    def end(self):
        """Setdown ncurses and restore the previous terminal environment."""
        curses.nocbreak()
        curses.endwin()

    def carousel(self):
        """It is the function that manages the slides and displays the correct
        one on the screen. Main loop of the program."""
        current_slide = 0
        while True:
            key = self.show(self.slides[current_slide])
            self.clear()
            if key == 'next':
                current_slide += 1
                if current_slide >= len(self.slides):
                    current_slide = len(self.slides) -1
            elif key == 'previous':
                current_slide -= 1
                if current_slide < 0:
                    current_slide = 0
            elif key == 'exit':
                break

    def show(self, slide):
        """In shows a slide. First it process the header, then the content of
        the slide and then the footer of the slide if requested."""
        self.process_header(slide.caption)
        while not slide.is_eos():
            self.process_line(slide.next_line(), slide.fgcolor, slide.bgcolor)
        self.reset(slide)
        if slide.pbar:
            self.process_footer(slide.nslide)
        return(self.get_key())

    def reset(self, slide):
        """Clears the screen and restarts the counter of lines for the last
        slide and for the carousel."""
        slide.eos = False
        slide.current_line = 0
        self.current_line = self.vindent

    def get_key(self):
        """Reads a key to move to the next slide, to the previous slide or to
        close the slide-show."""
        dkeys = {
            curses.KEY_RIGHT: 'next',
            curses.KEY_DOWN: 'next',
            ord(' '): 'next',
            curses.KEY_LEFT: 'previous',
            curses.KEY_UP: 'previous',
            27: 'exit', # code 27 corresponds to ESC key
            ord('q'): 'exit',
            ord('Q'): 'exit',
            ord('e'): 'exit',
            ord('E'): 'exit'
        }
        key = self.stdscr.getch()
        try:
            return(dkeys[key])
        except KeyError: 
            return None

    def process_line(self, line, fg, bg):
        """It process a line reading if some 'on-time' command mus be run (like
        --hline or --bold_on,...) or if it just need to be printed."""
        if self.raw_mode and not line.startswith('--raw_mode_off'):
            self.print_line(line)
        elif line.startswith('--hline'):
            self.hline()
        elif line.startswith('--bold_on'):
            self.mod['modifier'] = (True, curses.A_BOLD)
        elif line.startswith('--bold_off'):
            self.mod['modifier'] = (False, )
        elif line.startswith('--txt_color_on'):
            txt_fgcolor = line.replace('--txt_color_on ', '')
            self.txt_fgcolor = COLORS[txt_fgcolor]
            curses.init_pair(2, self.txt_fgcolor, self.txt_bgcolor)
            self.mod['colors'] = 2
        elif line.startswith('--txt_color_off'):
            self.mod['colors'] = 1
            self.txt_fgcolor = self.txt_fgcolor
        elif line.startswith('--left'):
            self.align = 'left'
        elif line.startswith('--center'):
            self.align = 'center'
        elif line.startswith('--right'):
            self.align = 'right'
        elif line.startswith('--raw_mode_on'):
            self.raw_mode = True
        elif line.startswith('--raw_mode_off'):
            self.raw_mode = False
        else:
            self.print_line(line)
        curses.init_pair(1, fg, bg)

    def print_line(self, line):
        """Takes a line of a slide and it fragments it to fits the screen. Then
        it shows each of the fragments."""
        width = self.twidth - 2 * self.hindent
        if self.raw_mode:
            lines = [line]
        else:
            lines = split_lines(line, width, self.align)
        for line in lines:
            if self.current_line < self.theight - self.vindent - 2:
                self.stdscr.move(self.current_line, self.hindent)
                if self.mod['modifier'][0]:
                    self.stdscr.addstr(line, 
                        curses.color_pair(self.mod['colors']) | self.mod['modifier'][1])
                else:
                    self.stdscr.addstr(line, curses.color_pair(self.mod['colors']))
                self.current_line += 1

    def clear(self):
        """Clears the screen on the terminal."""
        self.stdscr.clear()
        self.stdscr.refresh()

    def hline(self, char = "-"):
        """Draws an horizontal line."""
        width = self.twidth - 2 * self.hindent
        ln = char * width
        self.print_line(ln)

    def process_header(self, slide_caption):
        """Prints the head or the slide."""
        if slide_caption != "":
            self.print_line(slide_caption)
            self.hline()

    def process_footer(self, nslide):
        """Print the footer of the slide."""
        tslides = len(self.slides) - 1
        width = self.twidth - 2 * self.hindent
        nslide += 1
    
        nn = len(str(tslides))
        ns = str(nslide).rjust(nn, '0') + '/' +\
                str(tslides).rjust(nn, '0') + " "
    
        pp = int(round(nslide / (tslides * 1.0) * 100, 0))
        np = " " + str(pp).rjust(3, " ") + '%'

        wd = width - len(ns) - len(np) - 2
        pp = int(round(wd * pp / 100, 0))

        pbar = ns + '[' + '=' * pp + ' ' * (wd - pp) + ']' + np

        self.stdscr.move(self.theight - self.vindent - 1, self.hindent)
        self.stdscr.addstr(pbar)

    def create_portrait(self, sld):
        """Creates the first side of the carousel."""
        has_title = self.title != ''
        has_author = self.author != ''

        if not has_title and not has_author:
            return

        sld.add_line(" ")
        sld.add_line(self.title)
        sld.add_line("--hline")
        if self.subtitle != "":
            sld.add_line(self.subtitle)
        sld.add_line(" ")
        sld.add_line(" ")
        if self.author != "":
            sld.add_line(self.author)
            if self.affiliation != "":
                sld.add_line(self.affiliation)
        sld.add_line(" ")
        if self.date != "":
            sld.add_line(self.date)
        sld.set_pbar(False)
        self.slides = [sld] + self.slides

## ----------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(
        description = 'slide-show in terminal'
    )
    parser.add_argument('--input_file', 
        help = 'Input .kl file',
        action = 'store', 
        dest = 'input_file',
    )
    parser.add_argument('--fgcolor', 
        help = 'Foreground (text) color',
        action = 'store', 
        dest = 'fgcolor'
    )
    parser.add_argument('--bgcolor', 
        help = 'Background color',
        action = 'store', 
        dest = 'bgcolor'
    )

    args = parser.parse_args()

    if args.input_file is None:
        parser.print_help()
    
    if not args.fgcolor is None and not args.fgcolor in COLORS.keys():
        print 'Invalid color'
        print '   ', reduce(lambda x, y: x + ', '+ y, COLORS.keys())

    if not args.bgcolor is None and not args.bgcolor in COLORS.keys():
        print 'Invalid color'
        print '   ', reduce(lambda x, y: x + ', '+ y, COLORS.keys())

    x = KalosShow(args.input_file, 
        args.fgcolor if args.fgcolor is None else COLORS[args.fgcolor],
        args.bgcolor if args.bgcolor is None else COLORS[args.bgcolor]
    )
    try:
        x.start()
        x.carousel()
    except Exception, ex:
        x.end()
        raise ex
    finally:
        x.end()

## ----------------------------------------------------------------------------

if __name__ == '__main__':
    main()
